package com.sacti.baharudinafif.androiddev01.controller.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.sacti.baharudinafif.androiddev01.controller.TabLeft;
import com.sacti.baharudinafif.androiddev01.controller.TabMiddle;
import com.sacti.baharudinafif.androiddev01.controller.TabRight;

/**
 * Created by baharudinafif on 2/9/17.
 */

public class CustomPagerAdapter extends FragmentPagerAdapter {
    private int totalFragment;
    private final String TAG = "CustomPageAdapter";

    public CustomPagerAdapter(FragmentManager fm, int total){
        super(fm);
        this.totalFragment = total;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "getItem: " + position);
        switch (position) {
            case 0:
                TabLeft tabL = new TabLeft();
                return tabL;
            case 1:
                TabMiddle tabM = new TabMiddle();
                return tabM;
            case 2:
                TabRight tabR = new TabRight();
                return tabR;
            default:
                return null;
        }
    };

    @Override
    public int getCount() {
        return totalFragment;
    }
}
