# Android Development Class - Part 1

"Android Architecture: Make a happy life with Design Pattern"
Haloo Sov, ketemu lagi di kelas Android Dev Part I. Setelah di pertemuan sebelumnya kita membahas serba-serbi kebutuhan (prerequisite) dalam membangun Android Apps, kali ini kita bakal membahas tentang Design Pattern yang ada di Android mobile development. Design Pattern adalah sebuah konsep yang memberikan big picture dalam membangun Apps, sehingga kedepannya Sov Sov ga perlu pusing-pusing ketika mau nambah2 fitur ke Appsnya.
Sesi ini akan dibagi menjadi 3 bagian:

1. Design Pattern

2. Android Design Pattern: Model View Controller

3. Android SDK: View Pager

Repository ini adalah repository hasil workshop Part 1. Silahkan dicoba-coba dan kalo ada yg error jgn lupa berkabar ya, terima kasih.

Sampai jumpa di pertemuan selanjutanya :)