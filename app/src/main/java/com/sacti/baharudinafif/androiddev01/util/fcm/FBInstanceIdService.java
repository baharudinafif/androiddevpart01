package com.sacti.baharudinafif.androiddev01.util.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by baharudinafif on 2/10/17.
 */

public class FBInstanceIdService extends FirebaseInstanceIdService {
    private final String TAG = "FBInstanceIdService ";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, "onTokenRefresh: " + token);
    }
}

// frGcpZPcRrg:APA91bEnpHeG7LbtbiWPTg25FvQQxQtAPqaDPhzjPYtr_L2Xr9TxjiCF5l7qxe5Itj5QrGMIDtsaXu2K5NS7ku3Q2ESrKl5Ia2kvc2yphOY6WxomH8z-UwMSyjF9Tz7kykvJ6A7Uuz-i
