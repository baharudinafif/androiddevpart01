package com.sacti.baharudinafif.androiddev01.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.sacti.baharudinafif.androiddev01.R;
import com.sacti.baharudinafif.androiddev01.controller.adapter.CustomPagerAdapter;

public class HomeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{
    private final String TAG = "HomeActivity";
    private Context context;
    private Toolbar toolbar;
    private TabLayout tabs;
    private ViewPager pager;
    private Toast toast;
    private CustomPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        * ENABLE STRICT MODE
        * */
        /*
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
//                .penaltyDeath()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());
        */

        // Inflate View (Pasang Tampilan) activity_main.xml
        setContentView(R.layout.activity_main);

        context = getApplicationContext();
        tabs = (TabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.viewPager);

        tabs.addTab(tabs.newTab().setText("Left"));
        tabs.addTab(tabs.newTab().setText("Middle"));
        tabs.addTab(tabs.newTab().setText("Right"));
        tabs.addOnTabSelectedListener(this);

        toolbar = (Toolbar) findViewById(R.id.actionBar);
        toolbar.setTitle("Soval");
        toolbar.setTitleTextColor(ContextCompat.getColor(context, R.color.white));
        setSupportActionBar(toolbar);

        pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), tabs.getTabCount());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        toast = Toast.makeText(context, item.getTitle(), Toast.LENGTH_SHORT);
        toast.show();
        return false;
    }

    /*
     * Tab Layout
     ****/
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Log.i(TAG, "onTabSelected: " + tab.getPosition());
        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
